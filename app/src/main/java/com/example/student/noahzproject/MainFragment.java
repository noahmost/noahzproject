package com.example.student.noahzproject;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MainFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);

        ToDoInfo i1 = new ToDoInfo("n", "o", "a", "h");
        ToDoInfo i2 = new ToDoInfo("n", "o", "a", "h");
        ToDoInfo i3 = new ToDoInfo("n", "o", "a", "h");
        ToDoInfo i4 = new ToDoInfo("n", "o", "a", "h");

        activity.toDoInfos.add(i1);
        activity.toDoInfos.add(i2);
        activity.toDoInfos.add(i3);
        activity.toDoInfos.add(i4);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

updateUserInterface();

        return view;
    }

    public void updateUserInterface(){
        RedditPostAdapter adapter = new RedditPostAdapter(activityCallback);
        recyclerView.setAdapter(adapter);
    }
}

