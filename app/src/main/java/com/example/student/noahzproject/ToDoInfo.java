package com.example.student.noahzproject;

public class ToDoInfo {
    public String title;
    public String date;
    public String due;
    public String category;
    public ToDoInfo(String title, String date, String due, String category){
        this.title = title;
        this.date = date;
        this.due = due;
        this.category = category;
    }
}